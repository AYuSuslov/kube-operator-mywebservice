/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// MyWebServiceSpec defines the desired state of MyWebService
type MyWebServiceSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Service Parameters
	Name              string            `json:"name"`
	DockerImage       string            `json:"dockerImage"`
	DockerAccessCreds string            `json:"dockerAccessCreds,omitempty"`
	Replicas          int32             `json:"replicas,omitempty"`
	ResponseString    string            `json:"responseString,omitempty"`
	ServiceType       string            `json:"serviceType"`
	ServicePort       int32             `json:"servicePort,omitempty"`
	NodePort          int32             `json:"nodePort,omitempty"`
	PodLabels         map[string]string `json:"podLabels"`
	PodAnnotations    map[string]string `json:"podAnnotations,omitempty"`
}

// MyWebServiceStatus defines the observed state of MyWebService
type MyWebServiceStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	Name string `json:"name,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// MyWebService is the Schema for the mywebservices API
type MyWebService struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   MyWebServiceSpec   `json:"spec,omitempty"`
	Status MyWebServiceStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// MyWebServiceList contains a list of MyWebService
type MyWebServiceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []MyWebService `json:"items"`
}

func init() {
	SchemeBuilder.Register(&MyWebService{}, &MyWebServiceList{})
}
