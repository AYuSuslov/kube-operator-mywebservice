/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"time"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	servicev1 "my.ooma.com/myweb/api/v1"
)

// MyWebServiceReconciler reconciles a MyWebService object
type MyWebServiceReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

var workingNS string

//+kubebuilder:rbac:groups=service.my.ooma.com,resources=mywebservices,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=service.my.ooma.com,resources=mywebservices/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=service.my.ooma.com,resources=mywebservices/finalizers,verbs=update
//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the MyWebService object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.11.0/pkg/reconcile
func (r *MyWebServiceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	l := log.FromContext(ctx)
	// l.Info("Processing MyWebServiceReconciler")

	// Get CRD if exists
	service := &servicev1.MyWebService{}
	err := r.Get(ctx, req.NamespacedName, service)
	// Declare ConfigMap
	cm := &corev1.ConfigMap{}
	cm.Name = "index-html"
	cm.Namespace = workingNS
	if err != nil && client.IgnoreNotFound(err) == nil {
		l.Info("MyWebService resource not found. Object must be deleted")
		err = r.Client.Get(ctx, types.NamespacedName{Name: cm.Name, Namespace: workingNS}, cm)
		if !errors.IsNotFound(err) {
			l.Info("Deleting a ConfigMap if no MyWebService resource", "Configmap.Namespace", workingNS, "Configmap.Name", cm.Name)
			err = r.Delete(ctx, cm)
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	} else if err != nil {
		l.Error(err, "Failed to get MyWebService instance")
		return ctrl.Result{}, err
	}

	// Check if the configmap already exists, if not, create a new one
	workingNS = service.Namespace
	err = r.Client.Get(ctx, types.NamespacedName{Name: cm.Name, Namespace: workingNS}, cm)
	if err != nil && client.IgnoreNotFound(err) == nil {
		cm = &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "index-html",
				Namespace: workingNS,
			},
			Data: map[string]string{
				"index.html": service.Spec.ResponseString + "\n",
			},
		}
		l.Info("Creating a ConfigMap", "Configmap.Namespace", workingNS, "Configmap.Name", cm.Name)
		err = r.Client.Create(ctx, cm)
		if err != nil {
			l.Error(err, "Failed to create Configmap", "Configmap.Namespace", workingNS, "Configmap.Name", cm.Name)
			return ctrl.Result{}, err
		}
		// Configmap created successfully - return and requeue
		return ctrl.Result{Requeue: true}, nil
	} else if err != nil {
		l.Error(err, "Failed to get ConfigMap")
		return ctrl.Result{}, err
	}

	// Check for response string change, if yes, update Configmap
	newResponseString := service.Spec.ResponseString
	if cm.Data["index.html"] != newResponseString+"\n" {
		oldCMData := cm.Data["index.html"]
		cm.Data["index.html"] = newResponseString + "\n"
		l.Info("Updating a ConfigMap", "Configmap.Namespace", workingNS, "Configmap.Name", cm.Name)
		err = r.Client.Update(ctx, cm)
		if err != nil {
			l.Error(err, "Failed to update Configmap", "Configmap.Namespace", workingNS, "Configmap.Name", cm.Name)
			return ctrl.Result{}, err
		}
		// Configmap updated successfully, check for deployment update if needed
		if newResponseString == "" && oldCMData != "\n" {
			dep := &appsv1.Deployment{}
			dep.Name = "nginx"
			err = r.Client.Get(ctx, types.NamespacedName{Name: dep.Name, Namespace: workingNS}, dep)
			l.Info("Removing Configmap from Deployment", "Deployment.Namespace", dep.Namespace, "Deployment.Name", dep.Name)
			dep = r.deployDeleteCM(dep)
			err = r.Client.Update(ctx, dep)
			if err != nil {
				l.Error(err, "Failed to update Deployment", "Deployment.Namespace", dep.Namespace, "Deployment.Name", dep.Name)
				return ctrl.Result{}, err
			}
		}
		if newResponseString != "" && oldCMData == "\n" {
			dep := &appsv1.Deployment{}
			dep.Name = "nginx"
			err = r.Client.Get(ctx, types.NamespacedName{Name: dep.Name, Namespace: workingNS}, dep)
			l.Info("Adding Configmap to Deployment", "Deployment.Namespace", dep.Namespace, "Deployment.Name", dep.Name)
			dep = r.deployAddCM(dep)
			err = r.Client.Update(ctx, dep)
			if err != nil {
				l.Error(err, "Failed to update Deployment", "Deployment.Namespace", dep.Namespace, "Deployment.Name", dep.Name)
				return ctrl.Result{}, err
			}
		}
		// Configmap updated successfully. Return and requeue
		return ctrl.Result{Requeue: true}, nil
	}

	// Check if the deployment already exists, if not, create a new one
	found_dep := &appsv1.Deployment{}
	found_dep.Name = "nginx"
	err = r.Client.Get(ctx, types.NamespacedName{Name: found_dep.Name, Namespace: service.Namespace}, found_dep)
	if err != nil && errors.IsNotFound(err) {
		dep := r.deployMyWeb(service)
		l.Info("Creating a new Deployment", "Deployment.Namespace", dep.Namespace, "Deployment.Name", dep.Name)
		err = r.Client.Create(ctx, dep)
		if err != nil {
			l.Error(err, "Failed to create new Deployment", "Deployment.Namespace", dep.Namespace, "Deployment.Name", dep.Name)
			return ctrl.Result{}, err
		}
		// Deployment created successfully - return and requeue
		return ctrl.Result{Requeue: true}, nil
	} else if err != nil {
		l.Error(err, "Failed to get Deployment")
		return ctrl.Result{}, err
	}

	// Check for desired state of the deployments and change if needed, fields replicas, image, annotations, access creds
	dep_change := false
	size := service.Spec.Replicas
	labels := service.Spec.PodLabels
	annotations := service.Spec.PodAnnotations
	image := service.Spec.DockerImage
	secret := service.Spec.DockerAccessCreds
	if size == 0 {
		size = 1
	}

	// Deployment's Selector Matchlabels field is immutable after creation, needs to recreate Deployment
	for i := range labels {
		if found_dep.Spec.Template.ObjectMeta.Labels[i] != labels[i] {
			appl := &corev1.Service{}
			appl.Name = service.Spec.Name + "-service"
			err = r.Client.Get(ctx, types.NamespacedName{Name: appl.Name, Namespace: service.Namespace}, appl)
			l.Info("Changing Service Label Selector", "Service.Namespace", appl.Namespace, "Service.Name", appl.Name)
			appl.Spec.Selector[i] = labels[i]
			err = r.Client.Update(ctx, appl)
			if err != nil {
				l.Error(err, "Failed to update Service", "Service.Namespace", appl.Namespace, "Service.Name", appl.Name)
				return ctrl.Result{}, err
			}
			l.Info("Deleting Deployment to change Labels", "Deployment.Namespace", found_dep.Namespace, "Deployment.Name", found_dep.Name)
			err = r.Client.Delete(ctx, found_dep)
			return ctrl.Result{Requeue: true}, err
		}
	}

	if *found_dep.Spec.Replicas != size {
		found_dep.Spec.Replicas = &size
		dep_change = true
	}
	if found_dep.Spec.Template.Spec.Containers[0].Image != image {
		found_dep.Spec.Template.Spec.Containers[0].Image = image
		dep_change = true
	}
	for i := range annotations {
		if found_dep.Spec.Template.ObjectMeta.Annotations[i] != annotations[i] {
			found_dep.Spec.Template.ObjectMeta.Annotations[i] = annotations[i]
			dep_change = true
		}
	}
	if found_dep.Spec.Template.Spec.ImagePullSecrets == nil && secret != "" {
		found_dep.Spec.Template.Spec.ImagePullSecrets = append(found_dep.Spec.Template.Spec.ImagePullSecrets, corev1.LocalObjectReference{
			Name: secret,
		})
		dep_change = true
	} else if found_dep.Spec.Template.Spec.ImagePullSecrets != nil && secret == "" {
		found_dep.Spec.Template.Spec.ImagePullSecrets = nil
		dep_change = true
	} else if found_dep.Spec.Template.Spec.ImagePullSecrets != nil && found_dep.Spec.Template.Spec.ImagePullSecrets[0].Name != secret {
		found_dep.Spec.Template.Spec.ImagePullSecrets[0].Name = secret
		dep_change = true
	}

	if dep_change {
		l.Info("Updating Deployment", "Deployment.Namespace", found_dep.Namespace, "Deployment.Name", found_dep.Name)
		err = r.Client.Update(ctx, found_dep)
		if err != nil {
			l.Error(err, "Failed to update Deployment", "Deployment.Namespace", found_dep.Namespace, "Deployment.Name", found_dep.Name)
			return ctrl.Result{}, err
		}
		// Deployment Spec updated - return and requeue
		return ctrl.Result{Requeue: true}, nil
	}

	// Check if the application already exists, if not, create a new one
	found_appl := &corev1.Service{}
	found_appl.Name = service.Spec.Name + "-service"
	err = r.Client.Get(ctx, types.NamespacedName{Name: found_appl.Name, Namespace: service.Namespace}, found_appl)
	if err != nil && errors.IsNotFound(err) {
		appl := r.applMyWeb(service)
		l.Info("Creating a new Service", "Service.Namespace", appl.Namespace, "Service.Name", appl.Name)
		err = r.Client.Create(ctx, appl)
		if err != nil {
			l.Error(err, "Failed to create new Service", "Service.Namespace", appl.Namespace, "Service.Name", appl.Name)
			return ctrl.Result{}, err
		}
		// Service created successfully - return and requeue
		return ctrl.Result{Requeue: true}, nil
	} else if err != nil {
		l.Error(err, "Failed to get Service")
		return ctrl.Result{}, err
	}

	// Check for desired state of the application and change if needed
	appl_change := false
	applType := service.Spec.ServiceType
	applPort := service.Spec.ServicePort
	applNodePort := service.Spec.NodePort
	if applPort == 0 {
		applPort = 80
	}
	if found_appl.Spec.Ports[0].Port != applPort {
		found_appl.Spec.Ports[0].Port = applPort
		appl_change = true
	}
	if found_appl.Spec.Type == corev1.ServiceTypeNodePort {
		if found_appl.Spec.Ports[0].NodePort != applNodePort {
			found_appl.Spec.Ports[0].NodePort = applNodePort
			appl_change = true
		}
	}
	if found_appl.Spec.Type != corev1.ServiceType(applType) {
		found_appl.Spec.Type = corev1.ServiceType(applType)
		appl_change = true
	}

	if appl_change {
		l.Info("Updating Service", "Service.Namespace", found_appl.Namespace, "Deployment.Name", found_appl.Name)
		err = r.Client.Update(ctx, found_appl)
		if err != nil {
			l.Error(err, "Failed to update Service", "DService.Namespace", found_appl.Namespace, "Deployment.Name", found_appl.Name)
			return ctrl.Result{}, err
		}
		// Service Spec updated - return and requeue
		return ctrl.Result{Requeue: true}, nil
	}

	return ctrl.Result{RequeueAfter: (1 * time.Minute)}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *MyWebServiceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&servicev1.MyWebService{}).
		Owns(&appsv1.Deployment{}).
		Owns(&corev1.Service{}).
		Complete(r)
}

// Create Deployment
func (r *MyWebServiceReconciler) deployMyWeb(srv *servicev1.MyWebService) *appsv1.Deployment {
	var replicas int32
	if srv.Spec.Replicas == 0 {
		replicas = 1
	} else {
		replicas = srv.Spec.Replicas
	}
	labels := srv.Spec.PodLabels
	annotations := srv.Spec.PodAnnotations
	image := srv.Spec.DockerImage
	secret := srv.Spec.DockerAccessCreds
	dep := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "nginx",
			Namespace: srv.Namespace,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels:      labels,
					Annotations: annotations,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{{
						Image: image,
						Name:  srv.Spec.Name,
						Ports: []corev1.ContainerPort{{
							ContainerPort: 80,
						}},
					}},
				},
			},
		},
	}
	// Check if Secret have to be used in Deployment
	if secret != "" {
		dep.Spec.Template.Spec.ImagePullSecrets = append(dep.Spec.Template.Spec.ImagePullSecrets, corev1.LocalObjectReference{
			Name: secret,
		})
	}
	// Check if Configmap is needed
	if srv.Spec.ResponseString != "" {
		dep = r.deployAddCM(dep)
	}
	ctrl.SetControllerReference(srv, dep, r.Scheme)
	return dep
}

// Connect Configmap to the Deployment
func (r *MyWebServiceReconciler) deployAddCM(deploy *appsv1.Deployment) *appsv1.Deployment {
	deploy.Spec.Template.Spec.Containers[0].VolumeMounts = append(deploy.Spec.Template.Spec.Containers[0].VolumeMounts, corev1.VolumeMount{
		Name:      "nginx-index-file",
		MountPath: "/usr/share/nginx/html/",
	})
	deploy.Spec.Template.Spec.Volumes = append(deploy.Spec.Template.Spec.Volumes, corev1.Volume{
		Name: "nginx-index-file",
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: "index-html",
				},
			},
		},
	})
	return deploy
}

// Remove Configmap from the Deployment
func (r *MyWebServiceReconciler) deployDeleteCM(deploy *appsv1.Deployment) *appsv1.Deployment {
	deploy.Spec.Template.Spec.Containers[0].VolumeMounts = nil
	deploy.Spec.Template.Spec.Volumes = nil
	return deploy
}

func (r *MyWebServiceReconciler) applMyWeb(srv *servicev1.MyWebService) *corev1.Service {
	var servicePort int32
	serviceType := srv.Spec.ServiceType
	if srv.Spec.ServicePort == 0 {
		servicePort = 80
	} else {
		servicePort = srv.Spec.ServicePort
	}
	nodePort := srv.Spec.NodePort
	labels := srv.Spec.PodLabels
	serviceName := srv.Spec.Name

	appl := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      serviceName + "-service",
			Namespace: srv.Namespace,
		},
		Spec: corev1.ServiceSpec{
			Type:     corev1.ServiceType(serviceType),
			Selector: labels,
			Ports: []corev1.ServicePort{{
				Name: serviceName + "-listener",
				Port: servicePort,
				TargetPort: intstr.IntOrString{
					IntVal: 80,
				},
			}},
		},
	}
	if appl.Spec.Type == corev1.ServiceTypeNodePort {
		appl.Spec.Ports[0].NodePort = nodePort
	}
	ctrl.SetControllerReference(srv, appl, r.Scheme)
	return appl
}
